﻿using Keilaus;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeilausTest
{
    [TestFixture]
    public class PistelaskuriTest
    {
        private Pistelaskuri peli;

        [SetUp]
        public void SetUp()
        {
            peli = new Pistelaskuri();
        }

        [Test]
        public void VoiHeittääKouruun()
        {
            HeitäMonta(0, 20);

            Assert.AreEqual(0, peli.Tulos);
        }

        [Test]
        public void VoiHeittääKaikkiYkköiset()
        {

            HeitäMonta(1, 20);

            Assert.AreEqual(20, peli.Tulos);
        }

        [Test]
        public void VoiHeittääPaikon()
        {
            peli.Heitto(5);
            peli.Heitto(5);
            peli.Heitto(3);
            HeitäMonta(0, 17);
            Assert.AreEqual(16, peli.Tulos);
        }

        [Test]
        public void VoiHeittääKaadon()
        {
            peli.Heitto(10);
            peli.Heitto(3);
            peli.Heitto(4);
            HeitäMonta(0, 16);
            Assert.AreEqual(24, peli.Tulos);
        }

        [Test]
        public void VoiHeittääTäykyn()
        {
            HeitäMonta(10, 12);
            Assert.AreEqual(300, peli.Tulos);
        }
        
        private void HeitäMonta(int keilat, int heitot)
        {
            for (var i = 0; i < heitot; i++)
                peli.Heitto(keilat);
        }
    }
}
