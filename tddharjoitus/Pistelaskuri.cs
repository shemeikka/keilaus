﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keilaus
{
    public class Pistelaskuri
    {

        private int[] heitot = new int[21];
        private int nytHeitto = 0;

        public int Tulos
        {
            get
            {
                var tulos = 0;
                var heittoIndex = 0;
                for (var ruutu = 0; ruutu < 10; ruutu++)
                {
                    if (OnKaato(heittoIndex))
                    {
                        tulos += GetKaatoTulos(heittoIndex);
                        heittoIndex++;
                    }
                    else if (OnPaikko(heittoIndex))
                    {
                        tulos += GetPaikkoTulos(heittoIndex);
                        heittoIndex += 2;
                    }
                    else
                    {
                        tulos += GetTavallisTulos(heittoIndex);
                        heittoIndex += 2;
                    }
                }
                return tulos;

            }

        }

        public void Heitto(int keilat)
        {
            heitot[nytHeitto++] = keilat;
        }

        private bool OnPaikko(int heittoIndex)
        {
            return heitot[heittoIndex] + heitot[heittoIndex + 1] == 10;
        }

        private bool OnKaato(int heittoIndex)
        {
            return heitot[heittoIndex] == 10;
        }

        private int GetTavallisTulos(int heittoIndex)
        {
            return heitot[heittoIndex] + heitot[heittoIndex + 1];
        }

        private int GetPaikkoTulos(int heittoIndex)
        {
            return heitot[heittoIndex] + heitot[heittoIndex + 1] + heitot[heittoIndex + 2];
        }

        private int GetKaatoTulos(int heittoIndex)
        {
            return heitot[heittoIndex] + heitot[heittoIndex + 1] + heitot[heittoIndex + 2];
        }

    }
}

 /*   class Peli
    {
        private int keilat;

        public int Heitto(int keilat)
        {
            return keilat;
        }

        public int Tulos()
        {
            return keilat;
        }

    }
*/
